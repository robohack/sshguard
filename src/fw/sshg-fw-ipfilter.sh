#!/bin/sh
# sshg-fw-ipfilter
# This file is part of SSHGuard.

# this script will have the following parameters:
#
#  $1  the address to block (e.g. 192.168.0.12)
#  $2  the address family [see src/common/address.h] (e.g. 4 or 6)
#  $3  the netmask of the address to block


# XXX these should be set by 'configure'
IPFILTER_CMD=/sbin/ipf
IPFILTER_CTL=/etc/rc.d/ipfilter

fw_init() {
    :
}

fw_block() {
    FAM=""
    if [ -n "$2" ]; then
        FAM="-$2"
    fi
    echo "block in log quick proto tcp/udp from $1/$3 to any" | \
        ${IPFILTER_CMD} ${FAM} -A -f -
}

fw_release() {
    FAM=""
    if [ -n "$2" ]; then
        FAM="-$2"
    fi
    echo "block in log quick proto tcp/udp from $1/$3 to any" | \
        ${IPFILTER_CMD} ${FAM} -A -r -f -
}

# flush all rules and just reload those in the current system config file
#
# N.B.:  The list of attackers and their history (from sshg-blocker) is far more
# interesting and useful than the list of currently blocked addresses, since the
# latter can be derived from the former.
#
fw_flush() {
	${IPFILTER_CTL} reload
}

fw_fin() {
	:
}
